import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./main.css";
import TaskContainer from "./TaskContainer";
import KeyTaskContainer from "./KeyTaskContainer";
import Timer from "./Timer";

function Main(props) {
  const [pageShow, setPageShow] = useState(false); // <-- Added variable that determines if page should render.
  const [error, setError] = useState("");
  const [taskData, setTaskData] = useState({});
  const navigate = useNavigate();
  const [currentTaskID, setCurrentTaskID] = useState();
  const [listTask, setlistTask] = useState([
    <TaskContainer
      key="1"
      accountData={props.account}
      setTaskData={setTaskData}
    />,
  ]);
  // counter used to create unique key for taskContainer
  const [counter, setCounter] = useState(2);

  // POST new task upon a click to start timer && set Task_ID
  async function handleSubmit() {
    let taskDataPost = { ...taskData };
    delete taskDataPost.categoryName;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(taskDataPost),
      headers: {
        "Content-Type": "application/json",
      },
    };
    let url = `${process.env.REACT_APP_API_HOST}/api/tasks`;
    let response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newTask = await response.json();
      // set posted Task as the TaskID for the PUT request
      setCurrentTaskID(newTask.id);
      // console.log("created new task here:", newTask);
    } else {
      setError("Could Not POST new Task");
      console.log(error);
    }
  }

  // Functionality to be able to add and remove tasks
  function changeNumTaskContainers(change) {
    if (change === "up") {
      setCounter(counter + 1);
      // adds item to end of list (makes copy, doesn't mutate original)
      setlistTask(
        listTask.concat(
          <TaskContainer
            key={counter}
            accountData={props.account}
            setTaskData={setTaskData}
          />
        )
      );
    } else if (change === "down" && listTask.length > 1) {
      // removes last item from list, but keeps at least one task always shown
      setlistTask(listTask.slice(0, listTask.length - 1));
    }
  }

  // ---------------------------------- REDIRECT TO LOGIN -------------------------------------
  // On page load, checks to see if an account was passed through props.
  // If no account was passed, page is redirected to login.
  // If an account was passed, pageShow is set to true, which allows the page to render.

  useEffect(() => {
    if (typeof props.account !== "object") {
      console.log("user not logged in, redirect to login page");
      console.log(props.account);
      navigate("/login");
    } else if (props.account.id) {
      setPageShow(true);
    }
  }, [navigate, props.account]);

  // While it may seem like you can write this without using useEffect or a pageShow variable,
  // it's not good practice to use navigate() immediately on page render, as it can cause
  // some problems and won't work consistently.
  // -----------------------------------------------------------------------------------------

  if (pageShow) {
    // <-- Added conditional so that page does not render unless pageShow is true.
    return (
      <>
        <div className="maincontainer">
          <div>
            <button
              className="addtask"
              type="button"
              onClick={() => changeNumTaskContainers("up")}
            >
              Add Task
            </button>
            <button
              className="addtask"
              type="button"
              onClick={() => changeNumTaskContainers("down")}
            >
              Remove Task
            </button>
          </div>
          <div className="Tasks">
            <div>{listTask}</div>
          </div>
          <div className="Timer">
            <Timer
              handleSubmit={handleSubmit}
              taskData={taskData}
              currentTaskID={currentTaskID}
              setTaskData={setTaskData}
              accountData={props.account}
            />
            <div className="Key-Task">
              <KeyTaskContainer taskData={taskData} />
            </div>
          </div>
        </div>
        <div style={{ color: "white" }}>
          <h5>How to use the Hatch App</h5>
          <ul>
            <li>Add/Remove Tasks as desired</li>
            <li>Input 'Task Name', estimated pomodoros, category. </li>
            <li>
              Select the task you want to track by clicking 'Select this as Key
              Task'
            </li>
            <li>Click 'Start'!</li>
            <li>
              If you do not complete a Task, it will not be tracked in the
              Profile Page
            </li>
            <li>
              Enter the Profile page if you would like to adjust the timer
              length
            </li>
            <li>
              A Difficulty set to '1' results in 15 minutes of focus and a 15
              min break
            </li>
            <li>
              A Difficulty set to '2' results in 20 minutes of focus and a 10
              min break
            </li>
            <li>
              A Difficulty set to '3' results in 25 minutes of focus and a 5 min
              break
            </li>
          </ul>
        </div>
      </>
    );
  }
}

export default Main;
