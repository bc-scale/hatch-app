import React from "react";

function Footer() {
  return (
    <>
      <footer className="text-lg-start bg-dark text-light border-top">
        <div className="mx-5 text-md-start mt-3 pt-2">
          <div className="row justify-content-center mt-2">
            <div className="col-xl-3 mx-auto mb-2">
              <h6 className="text-uppercase fw-bold mb-4">Hatch App</h6>
              <p>An eggcelent way to get you ready and crackin'!</p>
            </div>
            <div className="col-xl-2 mx-auto mb-2">
              <h6 className="text-uppercase fw-bold mb-4">Contact Us</h6>
              <p>Amanda | Britton | Colin | Jay</p>
            </div>
            <div className="col-xl-2 mx-auto mb-2">
              <img src={`../HATCH.png`} alt="logo" width="200" height="auto" />
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;
