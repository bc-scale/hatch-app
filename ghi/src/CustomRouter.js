// ----------------------------- WHY YOU WANT TO USE THIS ---------------------------------
// Using this CustomRouter component as a middleman between App.js and your pages allows you
// to call on useToken() just once and pass the needed token and logging functions to each
// page component through props. You can also decode the token at this level to extract the
// account data of the logged in user, such as the user ID, username, email, etc. and pass
// that data down through props as well. This way, authorization related data is only called
// once at the top level and passed down to the child components, avoiding the need to call
// for user data in each individual component or to use useEffect to listen for when the token
// loads, which can causes all sorts of unexpected headaches on page render.

// Note that CustomRouter.js is needed as a child component of App.js and you cannot achieve
// the same functionality by calling useToken() directly inside of App.js because useToken()
// can only be called from inside the <AuthProvider> component, which is itself a child of
// App.js.
// ----------------------------------------------------------------------------------------

// import ErrorNotification from './ErrorNotification';
import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import NavBar from "./Nav";
import AccountForm from "./AccountForm";
import Main from "./Main";
import LoginForm from "./Login";
import LogoutForm from "./Logout";
import SignupForm from "./Signup";
import LandingPage from "./LandingPage";
import ProfilePage from "./Profile";
import Footer from "./Footer";
import { useToken } from "./useToken"; // <-- Added import for useToken
import jwt_decode from "jwt-decode"; // <-- Added import for jwt_decode to decode token into readable user data.
import "./App.css";

function CustomRouter() {
  // useToken is called here, and is the only time it should ever need to be called.
  // Any components that need to use the token or the logging functions can just get
  // them passed to them through props.
  const [token, login, logout, signup, update] = useToken();

  // An account variable is created to hold the account data extracted from decoding the access token.
  const [account, setAccount] = useState({
    id: null,
    username: null,
    email: null,
    first: null,
    last: null,
    avatar: null,
    reason: null,
    difficulty_id: null,
  });

  useEffect(() => {
    // If a token exists, meaning a user is logged in...
    if (!!token) {
      // decode the token and save the account data into "account".
      const tokenData = jwt_decode(token);
      setAccount(tokenData.account);
      // "account" is now an object through which you can access the logged in user information, such as:
      // account.id
      // account.username
      // account.reason
      // etc.
      // You can pass this object into your page components to access user information where needed.
    }
    // If a token does not exist (like after the user logs out)...
    else {
      // clear account.
      setAccount({
        id: null,
        username: null,
        email: null,
        first: null,
        last: null,
        avatar: null,
        reason: null,
        difficulty_id: null,
      });
    }
  }, [token]);

  return (
    <>
      <NavBar account={account} />
      <div className="container" style={{ minHeight: "80vh" }}>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="accounts/new" element={<AccountForm />} />
          <Route path="login/" element={<LoginForm login={login} />} />
          <Route path="logout/" element={<LogoutForm logout={logout} />} />
          <Route path="signup/" element={<SignupForm signup={signup} />} />
          <Route path="main/" element={<Main account={account} />} />
          <Route
            path="profile/"
            element={<ProfilePage account={account} update={update} />}
          />
        </Routes>
      </div>
      <Footer />
    </>
  );
}
export default CustomRouter;
