import { useToken, useAuthContext } from "./useToken";
import { useEffect } from "react";

function LogoutForm(props) {
  /* eslint-disable */
  const [token_, login, logout] = useToken();
  /* eslint-enable */
  const { token } = useAuthContext();

  useEffect(() => {
    logout();
  }, [token, logout]);
}

export default LogoutForm;
