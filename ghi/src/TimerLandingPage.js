import React, { useState, useEffect } from "react";
// import App from "./App.css";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom"; // <-- Added an import for react Link

// webpack recognizes paths without ./ to look into 'node_modules' folder
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

// import the Circular progressbar and its CSS styling
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

const TimerLandingPage = (props) => {
  const doneTime = 0;
  const startMinutes = 25;
  const breakMinutes = 5;

  // isActive will just let us know if the timer is running
  const [isActive, setIsActive] = useState(false);

  const [progressColor, setProgressColor] = useState("#4aec8c");
  const [progressMin, setProgressMin] = useState(startMinutes);

  const [minutes, setMinutes] = useState(startMinutes);
  const [seconds, setSeconds] = useState(0);
  const [breakTime, setBreakTime] = useState(false);

  // for modal
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // fix seconds to display in a typical timer fashion
  const fixSeconds = () => {
    if (seconds < 10) {
      return `0${seconds}`;
    } else if (seconds === 0) {
      return "00";
    }
    return seconds;
  };

  function startTimer() {
    setIsActive(true);
    setBreakTime(false);
    console.log("On timer start isActive:", isActive);
  }

  function reset() {
    // setSeconds(startTime);
    setIsActive(false);
    setMinutes(startMinutes);
    setSeconds(0);
    setBreakTime(false);
    console.log("on chicken out isActive:", isActive);
  }

  // if break desired. close modal. set minutes to break time.
  function startBreakTimer() {
    handleClose();
    setMinutes(breakMinutes);
    setIsActive(true);
    setBreakTime(true);
  }

  // revised timer using useEffect
  useEffect(() => {
    let workingTimer = null;
    let totalTime = minutes * 60;
    if (isActive) {
      // total time in seconds
      workingTimer = setInterval(
        () => {
          setMinutes(Math.floor(totalTime / 60));
          setSeconds(totalTime % 60);
          // lower totalTime by one second, every second
          totalTime--;

          // when task is done. Show modal
          if (totalTime < doneTime && breakTime === false) {
            clearInterval(workingTimer);
            handleShow();
            setProgressColor(blue);
            setProgressMin(breakMinutes);
          }
          // if its breaktime, and time runs out
          else if (totalTime < doneTime && breakTime === true) {
            clearInterval(workingTimer);
            setMinutes(startMinutes);
            setIsActive(false);
            setProgressColor(green);
            setProgressMin(startMinutes);
          }
        },
        // call function above XYZ milliseconds
        1
      );
    }
    // cleanup function when component unmounts (eg. you leave page)
    return () => clearInterval(workingTimer);

    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isActive, breakTime]);

  // if no break needed. reset timer
  function noBreakNeeded() {
    handleClose();
    setMinutes(startMinutes);
    setIsActive(false);
    setProgressColor(green);
    setProgressMin(startMinutes);
  }

  // circular progress bar information
  const blue = "#3388ff";
  const green = "#4aec8c";
  let percentage = (100 * (minutes * 60 + seconds)) / (progressMin * 60);

  return (
    <>
      <div
        className="m-1"
        style={{
          width: 300,
        }}
      >
        <div className="mb-5">
          <CircularProgressbar
            value={percentage}
            text={minutes + ":" + fixSeconds()}
            styles={buildStyles({
              textColor: "#FFFFFF",
              pathColor: progressColor,
              tailColor: "rgba(255,255,255,.2)",
            })}
          />
        </div>
        <div className="row my-2">
          <button
            className="button button-primary button-primary"
            onClick={startTimer}
          >
            {isActive ? "Get Crackin" : "Start"}
          </button>
          <button
            className="button button-primary button-primary"
            onClick={reset}
          >
            Reset
          </button>
        </div>
        <div style={{ display: "flex" }}>
          <button
            style={{
              width: "1500px",
              alignItems: "center",
            }}
            className="button button-primary"
          >
            <Nav.Link as={Link} to="login">
              Login for more features!
            </Nav.Link>
          </button>
        </div>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Pomodoro Finished</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Well done! You finished a pomodoro and deserve a break.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={noBreakNeeded}>
            No Break Needed
          </Button>
          <Button variant="primary" onClick={startBreakTimer}>
            Start break
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default TimerLandingPage;
