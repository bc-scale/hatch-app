import React, { useState, useEffect } from "react";
import "./main.css";

// import the Circular progressbar and its CSS styling
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

// webpack recognizes paths without ./ to look into 'node_modules' folder
import { Button, Modal } from "react-bootstrap";

const Timer = (props) => {
  const doneTime = 0;

  const { handleSubmit, taskData, currentTaskID, accountData } = props;

  // Setup Difficulty
  const userDifficultyId = accountData["difficulty_id"] - 1;

  const difficultyTimeOptions = { 0: [15, 15], 1: [20, 10], 2: [25, 5] };

  let startMinutes = difficultyTimeOptions[userDifficultyId][0];
  let breakMinutes = difficultyTimeOptions[userDifficultyId][1];

  const [minutes, setMinutes] = useState(startMinutes);
  const [seconds, setSeconds] = useState(0);
  const [breakTime, setBreakTime] = useState(false);
  const [progressColor, setProgressColor] = useState("#4aec8c");
  const [progressMin, setProgressMin] = useState(startMinutes);
  // isActive will just let us know if the timer is running
  const [isActive, setIsActive] = useState(false);
  const [error, setError] = useState(null);

  // States to show modal when task is completed
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // fix seconds to display in a typical timer fashion
  const fixSeconds = () => {
    if (seconds < 10) {
      return `0${seconds}`;
    } else if (seconds === 0) {
      return "00";
    }
    return seconds;
  };

  // Update task to be 'finished' when task timer goes to 0
  async function updateTask() {
    if (taskData.pom_finished > 0) {
      taskData.pom_finished -= 1;
    }
    // set task to be 'finished'
    taskData.finished = true;

    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(taskData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const task_id = currentTaskID;
    // console.log("task_id", currentTaskID);
    let url = `${process.env.REACT_APP_API_HOST}/api/tasks?task_id=${task_id}`;
    let response = await fetch(url, fetchConfig);

    if (response.ok) {
      // const updateTask = await response.json();
    } else {
      setError("Could Not update Task");
      console.log(error);
    }
  }

  async function startTimer() {
    await handleSubmit();
    setIsActive(true);
    setBreakTime(false);
  }

  // Resets Timer
  function reset() {
    setIsActive(false);
    setMinutes(startMinutes);
    setSeconds(0);
    setBreakTime(false);
  }

  // need to add error such that if the keytask is empty,
  // we need to prevent timer from running

  // if break desired -> close modal. set minutes to break time.
  function startBreakTimer() {
    handleClose();
    setMinutes(breakMinutes);
    setIsActive(true);
    setBreakTime(true);
  }

  // Timer Logic
  useEffect(() => {
    let workingTimer = null;
    let totalTime = minutes * 60;
    if (isActive) {
      // total time in seconds
      workingTimer = setInterval(
        () => {
          setMinutes(Math.floor(totalTime / 60));
          setSeconds(totalTime % 60);
          // lower totalTime by one second, every second
          totalTime--;

          // when task is done. Update task to Finish. Show modal
          if (totalTime < doneTime && breakTime === false) {
            clearInterval(workingTimer);
            updateTask();
            handleShow();
            setProgressColor(blue);
            setProgressMin(breakMinutes);
          }
          // if its breaktime, and time runs out, reset minutes to startime
          else if (totalTime < doneTime && breakTime === true) {
            clearInterval(workingTimer);
            setIsActive(false);
            setMinutes(startMinutes);
            setProgressColor(green);
            setProgressMin(startMinutes);
          }
        },
        // Set frequency to call function. Measured in milliseconds (eg. 1 is very fast)
        5
      );
    }
    // cleanup function when component unmounts (eg. you leave page)
    return () => clearInterval(workingTimer);

    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isActive, breakTime, taskData]);

  // if no break needed. reset timer
  function noBreakNeeded() {
    handleClose();
    setMinutes(startMinutes);
    setIsActive(false);
    setProgressColor(green);
    setProgressMin(startMinutes);
  }

  // circular progress bar information
  const blue = "#3388ff";
  const green = "#4aec8c";
  let percentage = (100 * (minutes * 60 + seconds)) / (progressMin * 60);

  return (
    <>
      <div>
        <CircularProgressbar
          value={percentage}
          text={minutes + ":" + fixSeconds()}
          styles={buildStyles({
            textColor: "#FFFFFF",
            pathColor: progressColor,
            tailColor: "rgba(255,255,255,.2)",
          })}
        />
      </div>
      <div>
        <div className="row">
          <button
            className="button button-primary button-primary"
            onClick={startTimer}
          >
            {isActive ? "Get Crackin" : "Start"}
          </button>
          <button
            className="button button-primary button-primary"
            onClick={reset}
          >
            Restart
          </button>
        </div>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header>
          <Modal.Title>Pomodoro Finished</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Well done! You finished a pomodoro and deserve a break.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={noBreakNeeded}>
            No Break Needed
          </Button>
          <Button variant="primary" onClick={startBreakTimer}>
            Start break
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Timer;
