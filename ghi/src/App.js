// ------------------- ROUTES HAVE BEEN MOVED ---------------------
// The navbar and routes have been moved to a new component called
// CustomRouter. This was done to make it easier to access, store,
// and call on user data to use in your page components. Check out
// CustomRouter.js to understand this better.
// ----------------------------------------------------------------

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { AuthProvider } from "./useToken";
import CustomRouter from "./CustomRouter";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  return (
    <AuthProvider>
      <BrowserRouter basename={basename}>
        <CustomRouter />
      </BrowserRouter>
    </AuthProvider>
  );
}
export default App;
