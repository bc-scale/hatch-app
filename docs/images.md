# More photos of the Hatch App

### Landing Page Timer

- Header includes links using React Router
- Header links adapt to whether or not user is logged in.
- Landing page provides a timer that users can use, but does not interact with the backend. For task tracking, users must create an account and sign in.

![Landing Page](/ghi/public/landing_page.jpg)

### Login

![Login](/ghi/public/login.jpg)

### Create User

![Create user](/ghi/public/create_user.jpg)

### Main Page

- Users can add and remove tasks. Remove Task button will NOT remove task, if only 1 task is present
- Setting a task as a 'Key Task', and clicking 'start' will result in a POST request, to create an 'unfinished' task.
- React Circular Progress Bar is used for timer

![Main page](/ghi/public/main.jpg)

### Timer Modal

- Modal presented upon completion of task
- Task completion results in PUT request to update Task as 'finished'
- Options provided include 'No Break Needed' which resets timer, or 'Start Break' which changes color of progress bar, and does break countdown

![Timer Modal](/ghi/public/modal.jpg)

### Profile Page

- page notes user specific data, based on tasks completed
- Button provided to Update Account (where user can change user information, and 'difficulty', which changes the timer focus/break lengths.
- uses CSS Grid & ChartJS

![Profile Page](/ghi/public/Profile_page.jpg)
