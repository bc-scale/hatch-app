from django.urls import path

from .vws import (
    api_user,
    api_category,
    api_tasks,
)

urlpatterns = [
    path("user/", api_user, name="api_user"),
    path("category/", api_category, name="api_category"),
    path("tasks/", api_tasks, name="api_tasks")
]