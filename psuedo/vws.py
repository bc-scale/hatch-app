from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encdrs import (
    UserEncoder,
    CategoryEncoder,
    TasksEncoder,
)
from .mods import User, Category, Tasks