from common.json import ModelEncoder

from .mods import User, Category, Tasks


class UserEncoder(ModelEncoder):
    model = User
    properties = [
        "id",
        "name",
        "username",
        "phone",
        "email",
        "user_picture",
        "reason",
    ]


class CategoryEncoder(ModelEncoder):
    model = Category
    properties = [
        "id",
        "name",
        "task",
    ]
    encoders = {
        "category": CategoryEncoder(),
    }


class TasksEncoder(ModelEncoder):
    model = Tasks
    properties = [
        "id",
        "user",
        "task",
        "category",
    ]
    encoders = {
        "tasks": TasksEncoder(),
    }
