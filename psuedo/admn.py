from django.contrib import admin
from .mods import User, Tasks, Category


admin.site.register(User)
admin.site.register(Tasks)
admin.site.register(Category)