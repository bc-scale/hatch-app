class User(models.Model):
    name = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    user_picture = models.URLField()
    reason = models.CharField(max_length=500)

class Tasks(models.Model):
    user = models.OneToMany(User, related_name="username", on_delete=models.CASCADE)
    task = models.CharField(max_length=200)
    category = models.ManyToMany(Category, related_name="tasks", on_delete=models.CASCADE)

class Category(models.Model):
    name = models.CharField(max_length=200)
    task = models.ManyToMany(Tasks, related_name="tasks", on_delete=models.PROTECT?)

Timer(CSS LANDING PAGE)



