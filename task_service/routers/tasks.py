from fastapi import APIRouter, Depends
from typing import List
from queries.tasks import (
    CategoriesOut,
    CategoriesRepository,
    DifficultyOut,
    DifficultyRepository,
    TaskIn,
    TaskOut,
    TaskRepository,
)

router = APIRouter()

# Categories: Read all
@router.get("/api/categories", response_model=List[CategoriesOut])
def get_categories(queries: CategoriesRepository = Depends()):
    return queries.get_categories()


# difficulties: Read all
@router.get("/api/difficulties", response_model=List[DifficultyOut])
def get_difficulties(queries: DifficultyRepository = Depends()):
    return queries.get_difficulties()


# Tasks: Read all tasks
@router.get("/api/tasks", response_model=List[TaskOut])
def get_tasks(queries: TaskRepository = Depends()):
    return queries.get_tasks()


# Tasks: Create single task
@router.post("/api/tasks", response_model=TaskOut)
def create_task(task: TaskIn, queries: TaskRepository = Depends()):
    print("task", task)
    # response.status_code = 400

    return queries.create_task(task)


# tasks: Delete single task
@router.delete("/api/tasks/{task_id}", response_model=bool)
def delete_task(task_id: int, queries: TaskRepository = Depends()):
    queries.delete_task(task_id)
    return True


# tasks: UPDATE single task
# "/api/tasks?task_id=2" Query parameter
@router.put("/api/tasks", response_model=TaskOut)
def update_task(
    # notice how these parameters are being passed onto the function below
    task_id: int,
    task_in: TaskIn,
    queries: TaskRepository = Depends(),
):
    # print('task_in---*', task_in)
    return queries.update_task(task_id, task_in)
