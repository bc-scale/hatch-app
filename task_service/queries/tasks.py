from pydantic import BaseModel
from typing import List
from queries.pool import pool


class Error(BaseModel):
    message: str


# retrieving categories
class CategoriesOut(BaseModel):
    # property and data type. These are type hints
    id: int
    name: str


# retrieving categories
class CategoriesRepository(BaseModel):
    # property and data type. These are type hints
    def get_categories(self) -> List[CategoriesOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                SELECT id, name
                FROM categories 
                """,
                )
                categories = []
                for record in result:
                    category = CategoriesOut(id=record[0], name=record[1])
                    categories.append(category)
                return categories


class DifficultyOut(BaseModel):
    id: int
    level: str
    work_time: int
    break_time: int


class DifficultyRepository:
    def get_difficulties(self) -> List[DifficultyOut]:
        # connect to database. use pools. with keyword similar to try/catch
        with pool.connection() as conn:
            # get a cursor (something to run sQL with)
            with conn.cursor() as db:
                result = db.execute(
                    """
                SELECT id, level, work_time, break_time
                FROM difficulty
                """,
                )
                difficulties = []
                for record in result:
                    difficulty = DifficultyOut(
                        id=record[0],
                        level=record[1],
                        work_time=record[2],
                        break_time=record[3],
                    )
                    difficulties.append(difficulty)
                return difficulties


class TaskIn(BaseModel):
    name: str
    start_time: str
    finished: bool
    category_id: int
    account_id: int
    pom_finished: int


class TaskOut(BaseModel):
    id: int
    name: str
    start_time: str
    finished: bool
    category_id: int
    account_id: int
    pom_finished: int


class TaskRepository(BaseModel):
    def get_tasks(self) -> List[TaskOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                SELECT id, name, start_time, finished, category_id, account_id, pom_finished
                FROM tasks
                """,
                )
                tasks = []
                for record in result:
                    task = TaskOut(
                        id=record[0],
                        name=record[1],
                        start_time=record[2],
                        finished=record[3],
                        category_id=record[4],
                        account_id=record[5],
                        pom_finished=record[6],
                    )
                    tasks.append(task)
                return tasks

    def create_task(self, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.name,
                    data.start_time,
                    data.finished,
                    data.category_id,
                    data.account_id,
                    data.pom_finished,
                ]
                cur.execute(
                    """
                  INSERT INTO tasks (name, start_time, finished, category_id, account_id, pom_finished)
                  VALUES (%s, %s, %s, %s, %s, %s)
                  RETURNING id, name, start_time,finished, category_id, account_id, pom_finished
                  """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record

    def delete_task(self, task_id):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                DELETE FROM tasks
                WHERE id = %s
                """,
                    [task_id],
                )

    def update_task(self, task_id, data):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.name,
                    data.start_time,
                    data.finished,
                    data.category_id,
                    data.account_id,
                    data.pom_finished,
                    task_id,
                ]
                cur.execute(
                    """
                UPDATE tasks
                SET 
                  name = %s,
                  start_time = %s,
                  finished = %s,
                  category_id = %s,
                  account_id = %s,
                  pom_finished= %s
                WHERE id = %s
                RETURNING id, name, start_time, finished, category_id, account_id, pom_finished
                """,
                    params,  # the order of params needs to match the SQL
                )
                record = None
                row = cur.fetchone()
                # print('row', row)
                # print('cur----*', cur)
                # print('cur-----**description---', cur.description)
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return record
