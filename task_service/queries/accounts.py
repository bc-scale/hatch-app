from pydantic import BaseModel
import psycopg
from .pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str
    email: str
    first: str
    last: str
    avatar: str
    reason: str
    difficulty_id: int


class AccountInWithIdAndHash(AccountIn):
    id: int
    hashed_password: str


class AccountOut(BaseModel):
    id: int
    username: str
    email: str
    first: str
    last: str
    avatar: str
    reason: str
    difficulty_id: int


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:
    def get(self, username: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT * FROM accounts
                    WHERE username = %s
                """,
                    [username],
                )

                row = cur.fetchone()
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]

                return record

    def create(self, info: AccountIn, hashed_password: str) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    INSERT INTO accounts (
                    username,
                    hashed_password,
                    email,
                    first,
                    last,
                    avatar,
                    reason,
                    difficulty_id
                    )
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING id, username, email, first, last, avatar, reason, difficulty_id
                    """,
                    [
                        info.username,
                        hashed_password,
                        info.email,
                        info.first,
                        info.last,
                        info.avatar,
                        info.reason,
                        info.difficulty_id,
                    ],
                )

                row = cur.fetchone()
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]

                return record

    def update(
        self, info: AccountInWithIdAndHash, hashed_password: str
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    UPDATE accounts
                    SET
                    username = %s,
                    hashed_password = %s,
                    email = %s,
                    first = %s,
                    last = %s,
                    avatar = %s,
                    reason = %s,
                    difficulty_id = %s
                    WHERE id = %s
                    RETURNING id, username, email, first, last, avatar, reason, difficulty_id
                    """,
                    [
                        info.username,
                        hashed_password,
                        info.email,
                        info.first,
                        info.last,
                        info.avatar,
                        info.reason,
                        info.difficulty_id,
                        info.id,
                    ],
                )
                row = cur.fetchone()
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]

                return record
