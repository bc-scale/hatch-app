from fastapi.testclient import TestClient
from jwtdown_fastapi.authentication import Token
import sys
import pathlib
import os


fastapi_dir = pathlib.Path(__file__).parent.parent.resolve()
abs_dir = os.path.abspath(fastapi_dir)
# adding Folder_2 to the system path
sys.path.append(abs_dir)

from main import app
from routers.accounts import AccountQueries, AccountOut, AccountToken, get_auth

client = TestClient(app)

account_out = AccountOut(
    id=1,
    username="string",
    email="string",
    first="string",
    last="string",
    avatar="string",
    reason="string",
    difficulty_id=1,
)

account = AccountToken(access_token="string", token_type="Bearer", account=account_out)

token = Token(access_token="string", token_type="Bearer")


class TestAuth:
    def hash_password(self, password):
        return "string"

    async def login(self, a, b, c, d):
        return token


async def get_test_auth():
    return TestAuth()


class MockAccountQueries:
    # def get(self, username):
    #   return account_out

    def create(self, item, hashed_password):
        return account_out


req_body_good = {
    "username": "string",
    "password": "string",
    "email": "string",
    "first": "string",
    "last": "string",
    "avatar": "string",
    "reason": "string",
    "difficulty_id": 1,
}

req_body_bad = {
    "username": "string",
    "password": "string",
    "email": "string",
    "first": "string",
    "last": "string",
    "avatar": "string",
    "reason": "string",
}

# TDD
# test something with logic
#   if no truck_id raise
# test post


def test_create_account():
    app.dependency_overrides[AccountQueries] = MockAccountQueries
    app.dependency_overrides[get_auth] = get_test_auth

    response = client.post("/api/accounts", json=req_body_good)

    assert response.status_code == 200
    assert response.json() == account

    # PLEASE NOTE: this is not quite right
    # the except is not catching the exception raised in MockMenuItemsQueries
    try:
        response = client.post("/api/accounts", json=req_body_bad)
    except:
        assert response.status_code == 422
        assert response.json() == None

    app.dependency_overrides = {}
