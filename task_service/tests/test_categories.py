from fastapi.testclient import TestClient
from main import app
from queries.tasks import CategoriesRepository, CategoriesOut

cat_get = CategoriesOut(id=1, name="string")
cat_get_list = [cat_get]

client = TestClient(app)


class MockCategoriesQueries:
    def get_categories(self):
        return cat_get_list


def test_get_categories():
    app.dependency_overrides[CategoriesRepository] = MockCategoriesQueries
    response = client.get("/api/categories")

    assert response.status_code == 200
    assert response.json() == cat_get_list

    app.dependency_overrides = {}
