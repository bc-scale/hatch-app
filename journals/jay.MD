## Jay's Journal

### Basic structure

The date of the entry
A list of features/issues that you worked on and who you worked with, if applicable
A reflection on any design conversations that you had
At least one ah-ha! moment that you had during your coding, however small

## Oct

**Today I worked on:**
**Reflection:**
**Lesson:**

## Oct - Thursday

**Today I worked on:**
Updated readme's, wireframing
Cleaned up code
**Reflection:**
Much of deployment still feels magical.
I still struggle with being able to see obstacles as opportunities, instead of hurdles. So much of this bootcamp has been an experience of not knowing something, working to study it, implement it in code, spending time unable to get it to work, and then getting things to work. That has been literally been the story of the entire bootcamp so far, the lesson I feel like I am still trying to learn is to recognize that there is a high probability that the problem in front of me will be solved, and in due time.
Our approach with git merges has also been confirmed by Andrew. We go to local main, pull, checkout devbranch, merge main, commit.
**Lesson:**
Related with algo's - the distinction between bubble sort and 

## Oct - Wednesday

**Today I worked on:**
Wrote up more of the readme
cleanup files - main.js, piechart.js, bargraph.js, keytaskcontainer.js, Timer.js
Added some labels to profile page graphs
Deployment - fixes include 'COPY files from the fast API directory (namely autheticator)' in the Dockerfile
Minor bug fixes
**Reflection:**
having much of the gitlab-ci.yml file really gives us the leg-up to finishing this.
**Lesson:**

## Oct - Tuesday

**Today I worked on:**
Cont Deployment in learn - updated our project to conform to notes presented there.
worked with Britton to finish pie chart for categories/tasks
setup a draft of the migration files for the backend deployment
**Reflection:**
it was super satisfying to get the piechart to work with Britton, it worked right when we got brought out of the breakout room
When things really get rolling, you can kind of get lost in coding/time spent
**Lesson:**
Want to use environment variables more regularly, versus hard-coded URL.
You learn from reading stuff. No surprise here.
Dockerfile is different than Dockerfile.dev

## Oct 24 - Monday

**Today I worked on:**
[x]circular progress bar in landing page
[x]finished one difficulty test
[x]fix difficulty issue
[x]Pull, and then -> go to accounts.py in routers and in line 100, delete the “/” after the word, accounts
[unfinished] fix pomodoro ticking down
**Reflection:**
Last few days, crunch time.
I wonder if how we have set things up, is good practice. There hasn't been much review from instructors, so we have just been going and seeing if things work... I just want to develop good habits here, but I feel like the time crunch prevents that from happening
**Lesson:**
react only triggers re-render if setState is used to update state. This is the root cause for the pomo's not updating...

## Oct 22 Saturday

**Today I worked on:**
[x]finished the progress bar to adjust both the percentage during work and break, as well as change colors based on state
[started] [problem] preparing difficutly based on user ID. Adjusted main, customRouter, Timer.
[started] write at least one unit test
[started] [problem] reduce pomodoros # in key_container <- need to figure out problem here
?? how do we rerender keyTaskcontainer after updatetask is fired in Timer.js
**Reflection:**
Being able to adjust state within conditionals is pretty cool.
I am still having issues with getting difficulties to show up correctly. I think it has to do with the async nature of the setter functions. I am settting difficulty in CustomRouters.
**Lesson:**

## Oct 21 Friday - no school

**Today I worked on:**
[x] first iteration of timer circular bar
[x] token cleanup (GET user_id in Task Container)
**Reflection:**
When using a new tool, it
**Lesson:**
for the CircularProgressbar, you need to npm install the library, import the component, and CSS

## Oct 20 - Thur

**Today I worked on:**
[x] fix Date() bug
[x] fix put request (related to getting task_id in Main.js
[x] pull/merge with main
**Reflection:**
Refactoring code that is already rigid is very challenging
I still struggle with being able to enjoy the process, instead I keep thinking about next steps, while not being able to enjoy the things that were completed. I do see this as a byproduct my 10 years in my previous job, where it was gogogo every day to reach a deadline.
Custom Router was added, which is a child of app.js. This custom router uses useEffect to retrieve token information, which is then passed down to the child components. This way we don't need to retrieve account information multiple times.
**Lesson:**
dispatch functions are async. You do not want to use a dispatch function to set state, and then use the state within the same function

## Oct 19

**Today I worked on:**
setting up a landing page timer component (that has no post/put logic).
Fixed bugs with timer, that would not show the modal popup after running a tasks after someone had pressed chicken out.
Worked on authentication/tokens/profile page with Amanda and Colin
[x] update tasks queries and POST method in main.js/taskcontainer to conform to new structure
[x] update tasks queries for PUT method
[x]token associated with post/put of task with user id
[x]shallow copy of taskData that can be used for POST, do not delete the category name / num pomodoros

**Reflection:**
writing code really does feel like writing bugs. You fix/make one thing, you break another
**Lesson:**
Reading the documentation clearly is important
creating a shallow copy using `let taskData = {...taskData}`

## Oct 18

**Today I worked on:**
Getting a modal setup for the timer, and getting the break timer functionality.
On occasions, being a second pair of eyes for Colin or Britton on their happenings
added functionality that allows you to add and remove task components
**Reflection:**
There are times when writing code feels like I am fixing issues in one place, and creating bugs in another...

**Lesson:**
For documentation, you sometimes have to just read thru all of it, even if it doesn't make sense, and simply going thru it, top to bottom, will help it make sense (advice from Andrew)
to use react bootstrap, you need to import CSS into app.js.
.concat and .slice are ways to create a shallow copy of a list, while adding and removing items from that list respectively. These methods are used for the adding and removing of task components

## Oct 17

**Today I worked on:**
Helped Colin on the profile page.
Trying to advance the timer functionality.
Established a 'stop' button for timer. This was really hard to figure out with setInterval surprisingly
**Reflection:**
Working with setInterval() is infuriating, it is so picky... really struggling with being able to STOP the timer when we want to.
**Lesson:**
pgAdmin can create tables using an GUI, not just thru SQL.
For setInterval within useEffect, a 'cleanup' function should be setup to clearInterval when the component unmounts.

## Oct 15

**Today I worked on:**
Showing Category Name in the KeyTaskContainer, instead of Category_id.
Updated Task Database Schema to include 'finished' attribute.
Changed StartTime to string - not sure if its meaningful to have an actual date data time.
Removed num_of_pomodoros as attribute in database
Revised Timer function to incorporate minutes
**Reflection:**
The layered function calls make me feel like there may be some inaccuracies in the time keeper (but computers may be doing things so quickly it may not matter).
Dealing with Time is not easy.
Timer has a slight delay
**Lesson:**
setInterval() built in function in JS allows you repeatedly call a function with a fixed time delay between each call (I've set call to repeat every 1000 milliseconds). You can 'stop' the setInterval using clearInterval().

## Oct 14

**Today I worked on:**
establish a 'key task' button in the taskContainer, which will populate the KeyTask container with Task data that a user can see visually. Clicking the Start button will perform a POST request with that task data
**Reflection:**
need to get better at personally dealing with ambiguity and uncertainty in these projects
**Lesson:**
button Tags, will require a 'type="button"' attribute. Without it, using that button will trigger a refresh in the page

## Oct 13

**Today I worked on:**
Set, update, and change state across Timer component, and a Task Container component. Main.js (the parent component) establishes state concerning a task. Hitting the timer start button, fires a submit function in the parent component. Within a TaskContainer component, I have an 'onChange' method that updates the timer data state, everytime a change is made for a specific task
One hurdle: cannot create multiple TaskContainers componetn anymore with this setup, since they will manipulate the state...
Hard coded some data to achieve first successful POST request to database with task information!
**Reflection:**
the configuration stuff is not very interesting to me... nor is straight up CSS. I do enjoy React though, and messing with state. Coding as a whole is enjoyable, but I really don't like configuration stuff (Docker, CI/CD, etc.)
**Lesson:**
Components can 'talk' with one another.
You can pass setter functions down as props. Interaction between parent and child components is possible in React

## Oct 12

**Today I worked on:**
Develop Timer logic in React, start up a task container.
**Reflection:**
Timer logic was inspired by code found online
the lecture on websockets was more interesting as it seems like a technology our team could use for our project.
**Lesson:**
the pipeline is mimiking the p

## Oct 11

**Today I worked on:**
finishing the endpoints for tasks and users
**Reflection:**
Received greater clarity and understanding of the git flow
greater clarity in the distinction between routers and queries.
Greater clarity of the function of psycopg. It is a postgres adapter for python
Our current put methods require us to PUT all the data attributes. There is a dynamic way (patch method) to deal with this that Josh shared with the class
**Lesson:**
many lessons today. query params are important. Endpoint paths do not need the {path_id} at the end, as query parameters seem to take care of this.
psycopg is the way you can use python to work with postgreSQL.

## Oct 10

**Today I worked on:**
merged/pushed all code as a team from all the work over the weekend
Running the SQL database using Beekeeper, fixed syntax errors.
Finalized endpoints for Categories and Difficulty in router/qeuries.

**Reflection:**
git merging/pushing was a headache, and it took a lot of our time

**Lesson:**
The order for SQL matters (top to bottom). If you have references, not when and where references are located.

## Oct 9

**Today I worked on:**
Finalized first draft of database.SQL
Setup basic structure for routers and queries for Categories
**Reflection:**
Not looking forward to messing with DATETIME
**Lesson:**

## Oct 7

**Today I worked on:**
Setting up the SQL database with Colin
**Reflection:**
**Lesson:**

## Oct 6, 2022

**Today I worked on:**
finishing the project setup, and got the docker container to work on my computer.
**Reflection:**
Dealing with ambiguity is always hard.
Much of the first steps are hard, we used CarCar as a way to test our understanding of the docker-compose file, and how it should be setup.
We started to define our roles a little bit more clearly today.
Amanda is spearheading the authentication
Britton is working on the frontend
Colin/Jay are working on the backend
We are still trying to figure out how we can best share our knowledge and experience together.
Seems like there are two ways to create that database, either thru migrations, or thru inserting SQL commands into pgAdmin or BeeKeeper.
**Lesson:**
Learned more about the docker-compose file today.
Volumes are where the data is stored. They need to be created using 'docker volume create volume_name'
typically - multiple databases are meant for microservices. If we are doing a monolith, there is no reason to have more than one database. You can have one database, and multiple tables - which is what we are doing.
the “database” is two things: the db-software and its data. The mongo/postgres image that we start with is the software and the data lives on the volume.
BeeKeeper and PGAdmin are ways that we can interact DIRECTLY with the database

## Oct 5, 2022

**Today I worked on:**
creating issues, and collaborated with Colin on setting up the PostgreSQL database thru our Dockerfile.
**Reflection:**
Recognizing the first step is not easy, it seems like we can go so many different ways. It seems like starting with defining the issues is a great way to get a handle on what we all need to accomplish, which will help us define what that first step should be.
**Lesson:**
There is a distinction between local branches and remote branches. The purpose of remote branches, is ultimately to merge to main.

## Oct 4, 2022

Today I worked on: project setup via Learn, as well as fixed deployment. The issues involved the trailing '/' in the PUBLIC URL, within 'variables' of the .gitlab-ci.yml. Our CORS_HOST value was also spelled incorrectly with a lowercase 'f'.
https://learn-2.galvanize.com/cohorts/3352/blocks/1893/content_files/build/01-crud/70-module-project.md
Reflection:
Lesson: You can merge to main via vscode.

## Oct 3, 2022

Today we all worked deployment of the project to heroku. We did a standup meeting in the morning, and discussed the status of the project, as well as goals for the day: deployment, api's, issues.
Reflection: The limited time on projects made it hard to get any real momentum on the project.
Lesson: The frontend of the application lives on GitLab, while the Backend lives on Heroku.

Project setup:
[x] .gitattributes
