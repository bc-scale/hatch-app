<!-- Amanda’s Journal Entries -->
<!-- Week 10/3 - 10/7 -->
Assigned certain tasks for each member to work on. Researched about Authentication and created the endpoints for the back-end authentication. Also, created the group’s logo.
<!-- Week 10/10 - 10/14 -->
Learning how the git merge process works and resolving merge conflicts. Researching about the front-end authentication and completed the front-end authentication. Created the forms for Login, Signup, and Logout. Added a use token file. Adjusted the authentication to enforce users to login to view main.
Added routes for Login, Logout, and Signup.
<!-- Week 10/17 - 10/21 -->
Resolve merge conflicts. Resolve failed pipelines. Got the main and profile page protected, so user needs to login with an account to view. Fixed and resolved some bugs.
<!-- Week 10/24 - 10/28 -->
Final project week!
Had a small take on CSS stuff and did some research.
Added some styling and colors for Login, Signup, and main pages. Added a footer.
Added the update functionality for the profile page, so that a user can update their data and have it changed within the website.
Fix some pipeline errors and merge conflicts. Also, fixed the footer and the spacing. Updated the navbar.
Came across some wbesite issues such as the update functionality not working, but had that fixed.
Started to work on the unit test to test out creation of accounts, but was stuck on what to do as the test did not pass. Got help from team members and instructors for the unit test.
Did more CSS/Bootstrap styling and fixed an issue of main not showing up when the page is refreshed.
Working on journal entries and README file.