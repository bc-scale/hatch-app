![Hatch](/ghi/public/HATCH.png)

- Amanda Deephano
- Colin Edwards
- Jay Go
- Britton West

---

### Design

- [API Design](/docs/api-design.md)
- [Wireframe Design](/docs/wireframe.md)

![react](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB) ![FastAPI](https://img.shields.io/badge/fastapi-109989?style=for-the-badge&logo=FASTAPI&logoColor=white) ![PostgreSQL](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white) ![Docker](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white) ![Heroku](https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white) ![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue) ![JavaScript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E) 

---

## The Hatch App in Action

- To read more about app functionality, please continue to bottom of the readme.
- Note that in gifs below, the timer is sped up, so you don't have to sit thru a 25 minute long gif :laughing:
- :camera:[ Click here for pictures of application, as well as brief explanations](/docs/images.md)

### Login

<img src="/ghi/public/login.gif" alt="login" width="800"/>

### Task creation and tracking

- gif starts at the user's profile page.
- Note that Pie Chart notes that (5) 'reading' tasks were completed. After a Pomodoro is completed, and we return to the profile page, the Pie Chart now reads (6) 'reading' tasks completed.

<img src="/ghi/public/full_feature.gif" alt="full_feature" width="800"/>

### Timer functionality (no break needed after task completed)

<img src="/ghi/public/landing_no_break_needed.gif" alt="landing_no_break_needed" width="800"/>

### Timer functionality (Break Requested)

<img src="/ghi/public/landing_break.gif" alt="landing_break" width="800"/>

### Timer functionality (Reset)

<img src="/ghi/public/landing_reset.gif" alt="landing_reset" width="800"/>

<!-- ![Hatch](/ghi/public/HATCH.png) -->

---

### Functionality

- First, find your 'why'. Then, use the Hatch app to help you focus to achieve your 'why' by using the pomodoro technique.
- The pomodoro technique involves breaking work into time intervals of 25 minutes of work, and a 5 minute subsequent break.
- Visitors of the page have the option to use the basic pomodoro timer (25 minutes of focus, and then a 5 minute break), or create and account/log-in to unlock more features.
- Creating a user involves inputting typical information, along with a profile picture (inserted as an url), and your 'why' (reason for using timer).
- When users log-in, they will be brought to the main page.
- User's at this point, can either choose to
  1. simply use the timer by clicking the 'start' button, or
  2. they can keep track of their tasks, adding tasks they want to work on. Required input fields include: Task Name, Category, and estimated Pomodoro's. These tasks must be marked as 'Key Task', by clicking the 'Key Task' button. After setting the key task, clicking the start button will start tracking this task. Tasks that have been completed (will reduce the # of estimated pomodoro timers, as well as log the finished task in the 'User Profile Page'.
- The final page is the User Profile Page. This page shows user information, and allows user to update their information as well.
  - A key feature of this page, are the graphs that keep track of completed pomodoro's. Two graphs are present to track how users are spending their focused time, and what days of the week they are most focused.

---

### How to use this project on local computer

You can enjoy the deployed site here!
https://50fifty.gitlab.io/hatch-app/

In order to run this project in a local development environment:

- git fork, and git clone this repo to your local computer
- CD into the directory where project is located
- Create the volumes required for the backend database, and run these commands:
  - `docker volume create postgres-data`
  - `docker volume create pg-admin` (if you intend on using pgAdmin)
- Build your Docker Container using:
  - `docker compose build`
  - If on M1 Mac:
  - `DOCKER_DEFAULT_PLATFORM=linux/amd64 docker-compose build`
- Spin up your Docker Container:
  - `docker compose up`
- Establish your PostgreSQL database thru your choice of PostGreSQL development platforms (pgAdmin, Beekeeper, etc.)
  - If using pgAdmin:
  - Access pgAdmin here: http://localhost:8082
    - username: hatchapp@hatch.com
    - password: password
  - Establish starting point for database:
    - Access `Tables` (Directory path: Servers/Hatch Database/Databases/Tasks/Schemas/public/Tables)
      - right click `Tables`, and select "Query Tool"
      - In your code editor, within the repo (Diretory: 'relational-data'), find 'Database.SQL', and `copy` all the SQL content
      - Within pgAdmin, `paste` the copied SQL content into the 'Query' Tool, and hit the 'Execute' button (looks like a play button)
        - This will create the database tables required for the applications, along with sample user accounts and tasks.
- Hop on the front end of the website, located on `localhost:3000` and enjoy using Hatch!

### URL's used for development:

React Front End

- localhost:3000 (access landing page, where user can use a basic pomodoro timer, or login/create account)
- localhost:3000/main (when a user is logged in, this page will allow a user to track specific tasks)
- localhost:3000/profile (when a user is logged in, this page will allow a user to see their user information, as well as charts that track their pomodoro usage. This page also allows users to update their user information)

FastAPI Swagger UI - accessing backend endpoints

- localhost:8000/docs
